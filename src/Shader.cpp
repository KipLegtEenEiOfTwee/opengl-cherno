#include "Shader.h"
#include "Renderer.h"

struct Shader::ShaderSources {
	std::string Vertex;
	std::string Fragment;
};

Shader::Shader(const std::string& path)
		: m_Path(path), m_RendererID(0)
{
	ShaderSources sources = ParseShader(path);
	m_RendererID = createShader(sources.Vertex, sources.Fragment);
}

Shader::~Shader()
{
	GLCall(glDeleteProgram(m_RendererID));
}

void Shader::Bind() const
{
	GLCall(glUseProgram(m_RendererID));
}

void Shader::Unbind() const
{
	GLCall(glUseProgram(0));
}

void Shader::SetUniform4f(const std::string& name, float v0, float v1, float v2, float v3)
{
	GLCall(glUniform4f(GetUniformLocation(name), v0, v1, v2, v3));
}

int Shader::GetUniformLocation(const std::string& name)
{
	if (m_UniformLocationCache.find(name) != m_UniformLocationCache.end()) {
		return m_UniformLocationCache[name];
	}

	GLCall(int loc = glGetUniformLocation(m_RendererID, name.c_str()));
	if (loc == -1) std::cout << "Uniform not found!" << std::endl;
	m_UniformLocationCache[name] = loc;
	return loc;
}

Shader::ShaderSources Shader::ParseShader(const std::string& path)
{
	std::ifstream stream(path);
	std::string line;
	std::stringstream ss[2];
	ShaderType type = ShaderType::NONE;

	while (getline(stream, line)) {
		if (line.find("#shader") != std::string::npos) {
			if (line.find("vertex") != std::string::npos) {
				type = ShaderType::VERTEX;
			} else if (line.find("fragment") != std::string::npos) {
				type = ShaderType::FRAGMENT;
			}
		} else {
			ss[(int) type] << line << '\n';
		}
	}
	return {ss[(int) ShaderType::VERTEX].str(), ss[(int) ShaderType::FRAGMENT].str()};
}

unsigned int Shader::compileShader(unsigned int type, const std::string& source)
{
	GLCall(unsigned int id = glCreateShader(type));
	const char *src = source.c_str();
	GLCall(glShaderSource(id, 1, &src, nullptr));
	GLCall(glCompileShader(id));

	int res;
	GLCall(glGetShaderiv(id, GL_COMPILE_STATUS, &res));
	if (res == GL_FALSE) {
		int len;
		GLCall(glGetShaderiv(id, GL_INFO_LOG_LENGTH, &len));
		auto *msg = (char *) alloca(len);
		GLCall(glGetShaderInfoLog(id, len, &len, msg));
		std::cout << "Error! "
		          << (type == GL_VERTEX_SHADER ? "Vertex" : "Fragment")
		          << " shader failed to compile!"
		          << std::endl;
		std::cout << msg << std::endl;
		GLCall(glDeleteShader(id));

		return 0;
	}

	return id;
}

/**
 *
 * @param vs Vertex Shader code.
 * @param fs fragment Shader code.
 * @return program id.
 */
unsigned int Shader::createShader(const std::string& vs, const std::string& fs)
{
	GLCall(unsigned int program = glCreateProgram());

	unsigned int vsp = compileShader(GL_VERTEX_SHADER, vs);
	unsigned int fsp = compileShader(GL_FRAGMENT_SHADER, fs);

	GLCall(glAttachShader(program, vsp));
	GLCall(glAttachShader(program, fsp));
	GLCall(glLinkProgram(program));
	GLCall(glValidateProgram(program));

	GLCall(glDeleteShader(vsp));
	GLCall(glDeleteShader(fsp));

	return program;;
}

void Shader::SetUniform1i(const std::string& name, int v0)
{
	GLCall(glUniform1i(GetUniformLocation(name), v0));
}

void Shader::SetUniformMat4f(const std::string& name, const glm::mat4& matrix)
{
	GLCall(glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, &matrix[0][0]));
}
