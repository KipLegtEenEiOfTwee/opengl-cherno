
#ifndef PROJECTNAME_TEXTURE_H
#define PROJECTNAME_TEXTURE_H

#include <stb_image.h>
#include "Renderer.h"

class Texture {
private:
	unsigned int m_RendererID;
	std::string m_FilePath;
	unsigned char *m_LocalBuffer;
	/**
	 * BPP = Bits Per Pixel.
	 */
	int m_Width, m_Height, m_BPP;
public:
	explicit Texture(const std::string& path);

	~Texture();

	void Bind(unsigned int slot = 0) const;

	void Unbind() const;

	inline int GetWidth() const { return m_Width; };

	inline int GetHeight() const { return m_Height; };
};


#endif //PROJECTNAME_TEXTURE_H
