//
// Created by Bas on 21-3-2018.
//

#include "Renderer.h"

void GLClearErrors()
{
	while (glGetError() != GL_NO_ERROR);
}

std::string GLErrorName(unsigned int error)
{
	switch (error) {
		CASE_ENUM_STRING(GL_NO_ERROR);
		CASE_ENUM_STRING(GL_INVALID_ENUM);
		CASE_ENUM_STRING(GL_INVALID_VALUE);
		CASE_ENUM_STRING(GL_INVALID_OPERATION);
		CASE_ENUM_STRING(GL_INVALID_FRAMEBUFFER_OPERATION);
		CASE_ENUM_STRING(GL_OUT_OF_MEMORY);
		default:
			return "";
	}
}

bool GLLogCall(const char *func, const char *file, int line)
{
	while (unsigned int error = glGetError()) {
		std::cout << "[OpenGL Error] "
		          << std::hex << "0x" << error << std::dec
		          << " " << GLErrorName(error)
		          << std::endl << "  -> "
		          << func << std::endl << "  -> "
		          << file << ":" << line
		          << std::endl;
		return false;
	}
	return true;
}

void Renderer::Draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const
{
	va.Bind();
	ib.Bind();
	shader.Bind();
	GLCall(glDrawElements(GL_TRIANGLES, ib.getCount(), GL_UNSIGNED_INT, nullptr));
}

void Renderer::Clear() const
{
	GLCall(glClear(GL_COLOR_BUFFER_BIT));
}
