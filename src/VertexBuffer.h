#ifndef PROJECTNAME_VERTEXBUFFER_H
#define PROJECTNAME_VERTEXBUFFER_H

class VertexBuffer {
private:
	unsigned int m_RendererID;
public:
	VertexBuffer(const void *data, unsigned int size);

	~VertexBuffer();

	void Bind() const;

	void Unbind()const;
};


#endif //PROJECTNAME_VERTEXBUFFER_H
