
#include "VertexBufferLayout.h"

VertexBufferLayout::VertexBufferLayout() : m_Stride(0) {}

void VertexBufferLayout::Push(unsigned int type, unsigned int count)
{
	switch (type) {
		case GL_FLOAT: PushFloat(count);
			break;
		case GL_UNSIGNED_INT: PushUnsignedInt(count);
			break;
		case GL_UNSIGNED_BYTE: PushUnsignedByte(count);
			break;
		default: ASSERT(false);
	}
}

void VertexBufferLayout::PushFloat(unsigned int count)
{
	m_Elements.push_back({GL_FLOAT, count, GL_FALSE});
	m_Stride += count * VertexBufferElement::GetSizeOfType(GL_FLOAT);
}

void VertexBufferLayout::PushUnsignedInt(unsigned int count)
{
	m_Elements.push_back({GL_UNSIGNED_INT, count, GL_FALSE});
	m_Stride += count * VertexBufferElement::GetSizeOfType(GL_UNSIGNED_INT);
}

void VertexBufferLayout::PushUnsignedByte(unsigned int count)
{
	m_Elements.push_back({GL_UNSIGNED_BYTE, count, GL_TRUE});
	m_Stride += count * VertexBufferElement::GetSizeOfType(GL_UNSIGNED_BYTE);
}
