#ifndef PROJECTNAME_MACRO_H
#define PROJECTNAME_MACRO_H

/**
 * Check if we're running vc++ or another compiler and define the correct debug breaker.
 */
#ifdef _MSC_VER
#define DEBUG_BREAK __debugbreak()
#else
#define DEBUG_BREAK __builtin_trap()
#endif

#define ASSERT(x) if (!(x)) DEBUG_BREAK
#define GLCall(x) GLClearErrors();x;ASSERT(GLLogCall(#x, __FILE__, __LINE__))
#define CASE_ENUM_STRING(x) case x:return #x

#endif //PROJECTNAME_MACRO_H
