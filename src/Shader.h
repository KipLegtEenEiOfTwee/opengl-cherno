
#ifndef PROJECTNAME_SHADER_H
#define PROJECTNAME_SHADER_H

#include <string>

#include <iostream>
#include <malloc.h>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <unordered_map>
#include <glm.hpp>

class Shader {
private:
	std::string m_Path;
	unsigned int m_RendererID;
	std::unordered_map<std::string, int> m_UniformLocationCache;

	struct ShaderSources;
public:
	Shader(const std::string& path);

	~Shader();

	void Bind() const;

	void Unbind() const;

	void SetUniform4f(const std::string& name, float v0, float v1, float v2, float v3);
	void SetUniform1i(const std::string& name, int v0);
	void SetUniformMat4f(const std::string& name, const glm::mat4& matrix);

private:

	int GetUniformLocation(const std::string& name);

	unsigned int createShader(const std::string& vs, const std::string& fs);

	unsigned int compileShader(unsigned int type, const std::string& source);

	ShaderSources ParseShader(const std::string& path);
};


#endif //PROJECTNAME_SHADER_H
