#ifndef PROJECTNAME_RENDERER_H
#define PROJECTNAME_RENDERER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <malloc.h>
#include <fstream>
#include <sstream>
#include <iomanip>

#include "VertexArray.h"
#include "Shader.h"
#include "IndexBuffer.h"

enum class ShaderType {
	NONE = -1, VERTEX = 0, FRAGMENT = 1,
};

void GLClearErrors();

std::string GLErrorName(unsigned int error);

bool GLLogCall(const char *func, const char *file, int line);

class Renderer {
public:
	void Clear() const;
	void Draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const;
};


#endif //PROJECTNAME_RENDERER_H
