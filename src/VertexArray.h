#ifndef PROJECTNAME_VERTEXARRAY_H
#define PROJECTNAME_VERTEXARRAY_H

#include "VertexBuffer.h"
#include "VertexBufferLayout.h"

class VertexArray {
private:
	unsigned int m_RendererID;
public:
	VertexArray();
	~VertexArray();

	void addBuffer(const VertexBuffer& vertexBuffer, const VertexBufferLayout& layout);

	void Bind() const;

	void Unbind()const;
};


#endif //PROJECTNAME_VERTEXARRAY_H
