#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <detail/type_mat.hpp>
#include <gtc/matrix_transform.hpp>
#include "Renderer.h"
#include "Texture.h"

int main()
{
	GLFWwindow *window;

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	/**
	 * Set the OpenGL version and profile.
	 */
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(640, 480, "Hello World", nullptr, nullptr);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	if (glewInit() != GLEW_OK)
		std::cout << "glewInit() returned an Error!" << std::endl;
	std::cout << glGetString(GL_VERSION) << std::endl;

	/**
	 * Extra scope to allow stack-allocated OpenGL objects to be destroyed before the OpenGL context is destroyed.
	 */
	{
		/**
		 * Data for a rectangle.
		 */
		float pos[16] = {
				-0.5f, -0.5f, 0.0f, 0.0f,
				0.5f, -0.5f, 1.0f, 0.0f,
				0.5f, 0.5f, 1.0f, 1.0f,
				-0.5f, 0.5f, 0.0f, 1.0f,
		};
		unsigned int indices[6] = {
				0, 1, 2,
				2, 3, 0,
		};

		/**
		 * Setup blending.
		 */
		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		/**
		 * Setup vertex array.
		 */
		VertexArray vertexArray;

		/**
		 * Setup vertex buffer.
		 */
		VertexBuffer vertexBuffer(pos, 4 * 4 * sizeof(float));

		/**
		 * Set vertex buffer attributes.
		 */
		VertexBufferLayout layout;
		layout.Push(GL_FLOAT, 2);
		layout.Push(GL_FLOAT, 2);
		vertexArray.addBuffer(vertexBuffer, layout);

		/**
		 * Setup index buffer.
		 */
		IndexBuffer indexBuffer(indices, 6);

		glm::mat4 proj = glm::ortho(-2.0f, 2.0f, -1.5f, 1.5f, -1.0f, 1.0f);

		/**
		 * Setup shaders.
		 */
		Shader shader("../res/shaders/Basic.glsl");
		shader.Bind();

		/**
		 * Setup uniforms.
		 */
		shader.SetUniform4f("u_color", 1.0f, 1.0f, 0.0f, 1.0f);
		shader.SetUniformMat4f("u_MVP", proj);

		/**
		 * Setup texture.
		 */
		Texture texture("../res/textures/thing.png");
		texture.Bind();
		shader.SetUniform1i("u_Texture", 0);

		/**
		 * Unbind all.
		 */
		vertexArray.Unbind();
		vertexBuffer.Unbind();
		indexBuffer.Unbind();
		shader.Unbind();

		Renderer renderer;

		float r = 0.0;
		float inc = 0.03;

		/**
		 * Draw loop.
		 */
		while (!glfwWindowShouldClose(window)) {
			renderer.Clear();

			/**
			 * Bind all.
			 */
			shader.Bind();

			/**
			 * Assign uniforms and call the Draw.
			 */
			shader.SetUniform4f("u_color", r + 0.2f, r + 0.2f, r, 1.0f);
			renderer.Draw(vertexArray, indexBuffer, shader);

			/**
			 * Increment the R value.
			 */
			if (r > 0.5f || r < 0.0f) {
				inc *= -1;
				r += inc;
			}
			r += inc;

			/**
			 * End of draw functions.
			 */
			glfwSwapBuffers(window);
			glfwPollEvents();
		}
	}

	glfwTerminate();
	return 0;
}