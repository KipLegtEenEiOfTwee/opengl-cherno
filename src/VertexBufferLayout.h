#ifndef PROJECTNAME_VERTEXBUFFERLAYOUT_H
#define PROJECTNAME_VERTEXBUFFERLAYOUT_H


#include <vector>
#include <GL/glew.h>
#include "macro.h"
//#include "Renderer.h"

struct VertexBufferElement {
	unsigned int type;
	unsigned int count;
	unsigned char normalized;

	static unsigned int GetSizeOfType(unsigned int type)
	{
		switch (type) {
			case GL_FLOAT: return sizeof(GLfloat);
			case GL_UNSIGNED_INT: return sizeof(GLuint);
			case GL_UNSIGNED_BYTE: return sizeof(GLubyte);
			default: ASSERT(false);
		}
	}
};

class VertexBufferLayout {
private:
	std::vector<VertexBufferElement> m_Elements;
	unsigned int m_Stride;
public:
	VertexBufferLayout();

	void Push(GLenum type, unsigned int count);

private:
	void PushFloat(unsigned int count);

	void PushUnsignedInt(unsigned int count);

	void PushUnsignedByte(unsigned int count);

public:
	inline const std::vector<VertexBufferElement> getElements() const { return m_Elements; }

	inline unsigned int getStride() const { return m_Stride; }


};


#endif //PROJECTNAME_VERTEXBUFFERLAYOUT_H
